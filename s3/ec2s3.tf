resource "aws_s3_bucket" "janevieve" {
  bucket        = "janevieve-bucket" #cannot have uppercase, and cannot have underscore
  force_destroy = true
  acl           = "private"
}

resource "aws_s3_bucket" "ayoki" {
  bucket        = "ayoki-bucket"
  force_destroy = true
  acl           = "private"
}

output "Bucket_name" {
  value = "${aws_s3_bucket.janevieve.bucket}"
}

