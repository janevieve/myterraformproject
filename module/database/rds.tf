resource "aws_db_instance" "my_database" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "oracle-ee"
  engine_version       = "11.2.0.4.v1"
  instance_class       = "db.t3.micro"
  name                 = "orcl"
  username             = "janevieve"
  password             = "janevieve"
 ##parameter_group_name = "default.mysql5.7"
  skip_final_snapshot = true
  publicly_accessible = true
  multi_az = true
}