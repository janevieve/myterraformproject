module "ec2module" {
 source = "./ec2/"
}
module "databasemodule" {
  source = "./database/"
}
module "vpcmodule" {
  source = "./vpc/"
}
module "s3module" {
  source = "./s3/"
}
module "IAMmodule" {
  source = "./IAM/" 
}
module "redshiftmodule" {
  source = "./redshift/" 
}
module "dynamodb" {
  source = "./dynamodb/" 
}

module "Aurora" {
  source = "./Aurora/" 
}
module "elasticache" {
  source = "./elasticache/"
  
}





