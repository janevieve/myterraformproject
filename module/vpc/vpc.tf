####################################################################
## This code is to create a vpc , private and public subnets      ##
## internet gateway,NAT gateway,route table and 2 security groups ##                              
####################################################################

#Create a VPC
resource "aws_vpc" "MY_VPC" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "dedicated"
  tags = {
    Name = "main"
  }
}

#Create public subnet in us-east-1a
resource "aws_subnet" "public" {
  vpc_id   = "${aws_vpc.MY_VPC.id}"
  cidr_block    = "10.0.1.0/24"
  availability_zone  = "us-east-1a"
  map_public_ip_on_launch = "true"
  tags = {
    Name = "Public_Subnet"
  }
}

#Create internet gateway and attaching it to the VPC
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.MY_VPC.id}"
  tags = {
    Name = "maingw"
  }
}

#Create Elastic ip
resource "aws_eip" "eip" {
  vpc = true
}

#Create NAT gateway 
resource "aws_nat_gateway" "nat_gw" {
  allocation_id = "${aws_eip.eip.id}"
  subnet_id     = "${aws_subnet.public.id}"
}

#Create a private subnet in us-east-1f
resource "aws_subnet" "private" {
  vpc_id            = "${aws_vpc.MY_VPC.id}"
  cidr_block        = "10.0.2.0/24"
  availability_zone = "us-east-1f"
  tags = {
    Name = "Private_Subnet"
  }
}

#Create the Route Table
resource "aws_route_table" "My_VPC_route_table" {
  vpc_id = "${aws_vpc.MY_VPC.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.gw.id}"
  }
}

#Associate the Route Table with the public subnet
resource "aws_route_table_association" "My_VPC_association" {
  subnet_id      = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.My_VPC_route_table.id}"
}

# Create the Security Group
resource "aws_security_group" "web_sg" {
  tags = {
    Name = "MY_WEBSERVER_SG"
  }
  ingress {
    to_port     = 80
    from_port   = 80
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    to_port     = 443
    from_port   = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    to_port     = 443
    from_port   = 443
    protocol    = "TCP"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#Allow TLS inbound traffic
resource "aws_security_group" "db_sg" {
  tags = {
    Name = "MY_DBSERVER_SG"
  }

  ingress {
    description = "TLS from VPC"
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
