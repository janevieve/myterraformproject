

#################################################################
## This code is to create a user, policy and attach the policy ##
## to the user. Also create roles and policies with outputs    ##
#################################################################

# User creation
resource "aws_iam_user" "MyUser" {
    name = "Janecloud"
}

resource "aws_iam_access_key" "Access_Key" {
  user = "${aws_iam_user.MyUser.name}"
}

#aws iam policy creation
resource "aws_iam_policy" "MyUserPolicy" {
    name = "MyCustomPolicy"
    policy = <<EOF
{
    "Version": "2012-10-17", 
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "lambda:CreateFunction",
                "lambda:TagResource",
                "lambda:GetLayerVersion",
                "lambda:PublishLayerVersion",
                "lambda:DeleteProvisionedConcurrencyConfig",
                "lambda:InvokeAsync",
                "lambda:GetAccountSettings",
                "lambda:GetFunctionConfiguration",
                "lambda:CreateEventSourceMapping",
                "lambda:GetLayerVersionByArn",
                "lambda:GetLayerVersionPolicy",
                "lambda:UntagResource",
                "lambda:PutFunctionConcurrency",
                "lambda:GetProvisionedConcurrencyConfig",
                "lambda:ListTags",
                "lambda:DeleteLayerVersion",
                "lambda:PutFunctionEventInvokeConfig",
                "lambda:DeleteFunctionEventInvokeConfig",
                "lambda:DeleteFunction",
                "lambda:GetAlias",
                "lambda:UpdateFunctionEventInvokeConfig",
                "lambda:UpdateEventSourceMapping",
                "lambda:GetEventSourceMapping",
                "lambda:InvokeFunction",
                "lambda:GetFunction",
                "lambda:UpdateFunctionConfiguration",
                "lambda:UpdateAlias",
                "lambda:UpdateFunctionCode",
                "lambda:GetFunctionConcurrency",
                "lambda:GetFunctionEventInvokeConfig",
                "lambda:PutProvisionedConcurrencyConfig",
                "lambda:DeleteAlias",
                "lambda:PublishVersion",
                "lambda:DeleteFunctionConcurrency",
                "lambda:DeleteEventSourceMapping",
                "lambda:GetPolicy",
                "lambda:CreateAlias"
            ],
            "Resource": "*",
            "Condition": {
                "IpAddress": {
                    "aws:SourceIp": "192.168.1.10/24"
                },
                "BoolIfExists": {
                    "aws:MultiFactorAuthPresent": "true"
                }
            }
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "s3:*",
                "ec2:*"
            ],
            "Resource": "*"
        }
    ]
}
 EOF
}

# Attaching the policy to the user Jane created above. 
resource "aws_iam_policy_attachment" "policybind" {
    name = "attachment"
    users = ["${aws_iam_user.MyUser.name}"]
    policy_arn = "${aws_iam_policy.MyUserPolicy.arn}"
}

#  Creating an IAM role
resource "aws_iam_role" "role" {
  name = "test-role"

  assume_role_policy = <<EOF
{
      "Version": "2012-10-17",
      "Statement": [
        {
          "Action": "sts:AssumeRole",
          "Principal": {
          "Service": "ec2.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
        }
      ]
    }
EOF
}

# Creating an IAM policy

resource "aws_iam_policy" "policy" {
  name        = "tt-policy"
  description = "A test policy"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "ec2:Describe*"
      ],
      "Effect": "Allow",
      "Resource": "*"
    }
  ]
}
EOF
}

# Attaching the policy to the iam role. 
resource "aws_iam_role_policy_attachment" "test-attach" {
  role       = "${aws_iam_role.role.name}"
  policy_arn = "${aws_iam_policy.policy.arn}"
}

# Creating outputs

output "IAM_USER"{
    value = "${aws_iam_user.MyUser.name}"
}

output "ACCESS_KEY" {
    value = "${aws_iam_access_key.Access_Key.id}"
}

output "POLICY_KEY" {
    value = "${aws_iam_policy.MyUserPolicy.name}"
}

output "SECRET" {
  value = "${aws_iam_access_key.Access_Key.secret}"
}