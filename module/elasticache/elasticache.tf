
##################################################################
## This code is to create an elasticcache , memcached and redis ##                              
###################################################################


resource "aws_elasticache_cluster" "nairobi" {
  cluster_id           = "cluster-nairobi"
  engine               = "memcached"
  node_type            = "cache.m4.large"
  num_cache_nodes      = 2
  #parameter_group_name = "default.memcached1.4"
  port                 = 11211
}

resource "aws_elasticache_cluster" "kisumu" {
  cluster_id           = "kisumu-cluster"
  engine               = "redis"
  node_type            = "cache.m4.large"
  num_cache_nodes      = 1
  #parameter_group_name = "default.redis3.2"
  engine_version       = "3.2.10"
  port                 = 6379
}


