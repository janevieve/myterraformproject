# providers for both source and destination

provider "aws" {
  access_key = "AKIA5QYYZTWUSZXLWIRS"
  secret_key = "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"
  alias  = "east"
  region = "us-east-1"
}

provider "aws" {
  access_key = "AKIA5QYYZTWUSZXLWIRS"
  secret_key = "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"
  region = "us-west-2"
}

# S3 bucket to bucket Cross Region Replocation 

resource "aws_iam_role" "crr_role" {
  name = "s3-crr-role"

  assume_role_policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "s3.amazonaws.com"
      },
      "Effect": "Allow",
      "Sid": ""
    }
  ]
}
POLICY
}

resource "aws_iam_policy" "crr_policy" {
  name = "s3-crr-policy"

  policy = <<POLICY
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "s3:GetReplicationConfiguration",
        "s3:ListBucket"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.mango.arn}"
      ]
    },
    {
      "Action": [
        "s3:GetObjectVersion",
        "s3:GetObjectVersionAcl"
      ],
      "Effect": "Allow",
      "Resource": [
        "${aws_s3_bucket.mango.arn}/*"
      ]
    },
    {
      "Action": [
        "s3:ReplicateObject",
        "s3:ReplicateDelete"
      ],
      "Effect": "Allow",
      "Resource": "${aws_s3_bucket.papaya.arn}/*"
    }
  ]
}
POLICY
}

resource "aws_iam_role_policy_attachment" "crr_policy_attachment" {
  role       = "${aws_iam_role.crr_role.name}"
  policy_arn = "${aws_iam_policy.crr_policy.arn}"
}

# Creating S3 buckets and enabling versioning

resource "aws_s3_bucket" "papaya" {
  bucket = "janevieve-prod-c"
  region = "us-west-2"

  versioning {
    enabled = true
  }
}


resource "aws_s3_bucket" "mango" {
  provider = "aws.east"
  bucket   = "janevieve-prod-a"
  acl      = "private"
  region   = "us-east-1"

  versioning {
    enabled = true
  }

  replication_configuration {
    role = "${aws_iam_role.crr_role.arn}"

    rules {
      id     = "foobar"
      prefix = "foo"
      status = "Enabled"

      destination {
        bucket        = "${aws_s3_bucket.papaya.arn}"
        storage_class = "STANDARD"
      }
    }
  }
}

output "Bucket1 Name" {
  value = "${aws_s3_bucket.mango.bucket}"
}

output "Bucket3 Name" {
  value = "${aws_s3_bucket.papaya.bucket}"
}