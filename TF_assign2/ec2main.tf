//This are the resources used to create 3 EC2 instances
resource "aws_instance" "web" {
    ami = "${var.ami}"
    instance_type = "${var.instance_type}"
    tags {
        Name = "MANGO"
        Env = "Prod"
    }
}
resource "aws_instance" "app" {
ami = "${var.ami}"
    instance_type = "${var.instance_type}"
    tags {
        Name = "DELUXE"
        Env = "Test"
    }
}    
resource "aws_instance" "file" {
ami = "${var.ami}"
    instance_type = "${var.instance_type}"
    tags {
        Name = "ACTIVE"
        Env = "Dev"
    }
 }
 //This section is used for the purpose of getting each EC2 instances ID   
output "AWS_InstanceId" {
 value = "${aws_instance.web.id}"
}
output "InstanceId" {
value = "${aws_instance.app.id}"
}
 
output "Instance_Id"{
 value = "${aws_instance.file.id}"
}