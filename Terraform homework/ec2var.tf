//This section describes the AMI needed to run on the EC2 instance
variable "ami"{
    default = "ami-0323c3dd2da7fb37d"
}
//The vpc used was the default vpc from the aws mgt console
variable "vpc_id" {}


//The output section is used for the purpose of getting the instance ID and vpc ID

output "InstanceId"  {
  value = "${aws_instance.web.id}"
}
output "aws_vpc" "vpc" {
  value = "${var.vpc_id}"
}






