//This are the resources or attribute used to create the 3 ec2 instance
resource "aws_instance" "web" {
     ami = "ami-0323c3dd2da7fb37d"
    instance_type = "t2.micro"
    tags {
        Name = "MANGO"
        Env = "Prod"
    }
}
resource "aws_instance" "app" {
ami = "ami-0323c3dd2da7fb37d"
    instance_type = "t2.micro"
    tags {
        Name = "DELUXE"
        Env = "Test"
    }

}
    
resource "aws_instance" "file" {
ami = "ami-0323c3dd2da7fb37d"
    instance_type = "t2.micro"
    tags {
        Name = "ACTIVE"
        Env = "Dev"
    }

 }
     
output "AWS_InstanceId"  {
 value = "${aws_instance.web.id}"
 value = "${aws_instance.app.id}"
 value = "${aws_instance.file.id}"
}


