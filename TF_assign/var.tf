//This section describes the AMI needed to run on the Ec2 instance
variable "ami"{
      default = "ami-0323c3dd2da7fb37d"
}
//Below are the resources or attributes used to create the ec2 instance
resource "aws_instance" "web" {
    ami = "${var.ami}"
    instance_type = "t2.micro"
    tags {
        Name = "Jane"
        Env = "Test"
    }  

resource "aws_vpc" "my_vpc" {
        cidr_block = "10.2.0.0/16" 
}
resource "aws_subnet" "my_subnet" {
  vpc_id            = "${aws_vpc.my_vpc.id}"
  cidr_block        = "172.31.0.0/16"
}
  

//This section is used for the purpose of getting the instance id 
output "aws_instance" {
  value = "${aws_instance.web.id}"
}
}