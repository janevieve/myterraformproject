resource "aws_instance" "APS2_WebServer2" {
    ami = "ami-09d95fab7fff3776c"
    instance_type = "t2.micro"
    tags = {
       Name = "APS2route53"
       Env = "Dev"
   }
user_data = <<EOF
#!/bin/bash
yum update -y
yum install httpd -y
service httpd start
chkconfig httpd on
cd /var/www/html
echo "<html><h1>Hello Activeskyagte Students! This is the North Virginia Web Server 2</h1></html>" > index.html
EOF
}

output "instance_id" {
  value = "${aws_instance.APS2_WebServer2.id}"
}
