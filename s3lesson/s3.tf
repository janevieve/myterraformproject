# Create an S3 bucket.
resource "aws_s3_bucket" "mybucket" {
    bucket = "dibo-stay-home"
    acl = "private"
}

#Attching an s3 bucket policy to allow any user to be able to read objects in the bucket. 
resource "aws_s3_bucket_policy" "mybucket" {
  bucket = "${aws_s3_bucket.mybucket.id}"

  policy = <<POLICY
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Sid":"PublicRead",
      "Effect":"Allow",
      "Principal": "*",
      "Action":["s3:GetObject"],
      "Resource":["arn:aws:s3:::dibo/*"]
    }
  ]
}
POLICY
}


#output the name of the bucket.
output "bucketname" {
    value = "${aws_s3_bucket.mybucket.bucket}"
}