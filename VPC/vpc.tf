
resource "aws_vpc" "myvpc" {
  cidr_block = "10.0.0.0/16"
  instance_tenancy = "dedicated"
    tags = {
    Name = "main"
  }
}

resource "aws_subnet" "public" {
    vpc_id = "${aws_vpc.myvpc.id}"
    cidr_block = "10.0.16.0/20"
    availability_zone = "us-east-1a" 
    map_public_ip_on_launch = "true"
    tags = {
    Name = "Public_Subnet"
 }
}
resource "aws_internet_gateway" "gw" {
  vpc_id = "${aws_vpc.myvpc.id}"
    tags = {
    Name = "maingw"
  }
}
 resource "aws_subnet" "private"{
     vpc_id = "${aws_vpc.myvpc.id}"
     cidr_block = "10.0.1.0/20"
     availability_zone = "us-east-1b"
     tags = {
         Name = "Private_Subnet"
     }
 }

  output "VPC_ID" {
    value = "${aws_vpc.myvpc.id}"     
  }
  output "Public Subnet id" {
  value = "${aws_subnet.public.id}"
  }
  output "Private Subnet id" {
  value = "${aws_subnet.private.id}"
  }