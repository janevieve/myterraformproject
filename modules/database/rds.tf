resource "aws_db_instance" "mydatabase" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "mydb"
  username             = "janevieve"
  password             = "Password123"
  parameter_group_name = "default.mysql5.7"
  skip_final_snapshot = true
}