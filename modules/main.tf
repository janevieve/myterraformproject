module "ec2module" {
  source = "./ec2/"
}

module "databasemodule" {
  source = "./database/"
}

module "vpcmodule" {
  source = "./vpc/"
}

module "s3module" {
  source = "./s3/"
}
module "iammodule" {
  source = "./IAM/"
  
}


