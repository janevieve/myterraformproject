resource "aws_instance" "webserver" {
        instance_type = "t2.micro"
        tags = {
            Name = "DELUXE"
            Env = "Dev"

        }
}  
resource "aws_vpc" "my_vpc" {
        cidr_block = "10.0.0/16"  
}
resource "ami" "my_ami" {
        ami = "ami-0323c3dd2da7fb37d"
  
}


output "VPCID" {
  value = "${aws_vpc.my_vpc.id}"
}
output "ami" {
  value = "${ami.my_ami}"
}

output "instanceId" {
  value = "${aws_instance.webserver.instance}"
}


